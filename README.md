# AdGuard Home with Unbound
AdGuard Home with Unbound docker-compose.yml

AdGuard Home: [Github](https://github.com/AdguardTeam/AdGuardHome) | [DockerHub](https://hub.docker.com/r/adguard/adguardhome)

»Free and open source, powerful network-wide ads & trackers blocking DNS server.«

klutchell/unbound: [Github](https://github.com/klutchell/unbound-docker) | [DockerHub](https://hub.docker.com/r/klutchell/unbound)

»Unofficial unbound multiarch docker image. Unbound is a validating, recursive, and caching DNS resolver.«

[I](https://bln41.win/blog/adguard-home-mit-unbound) use this combination with a Raspberry Pi 4b (Debian 11 / bullseye / aarch64).
